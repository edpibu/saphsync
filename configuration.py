from urllib.parse import urljoin

import keyring
import yaml


class Configuration:
    """Regroup app configuration"""

    def __init__(self, yaml_file):
        with open(yaml_file, 'r') as f:
            config = yaml.load(f)
            self.base_url = config['base_url']
            self.calendars_url = [urljoin(self.base_url, r) for r in config['calendars_url']]
            self.login = config['login']
            self.selected_groups = config['selected_groups']

            # Load password
            self.password = keyring.get_password('sogo', self.login)
