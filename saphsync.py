import logging

from icalendar import Calendar

from configuration import Configuration
from filters import filter_date, filter_group
from icalendar_tools import OnlineCalendar

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
config = Configuration('config.yml')

# Create master calendar
calendar = Calendar()
calendar.add('version', '2.0')
calendar.add('prodid', '-//SaphSync//EN')
calendar.add('method', 'PUBLISH')
calendar.add('x-wr-calname', 'SaphSync')

# Fetch online calendar and copy components
for url in config.calendars_url:
    online_calendar = OnlineCalendar(url, config.login, config.password)

    # Copy timezone definitions
    tz = online_calendar.get_timezone()
    calendar.add_component(tz)

    # Copy events
    for event in online_calendar.get_events():
        # If it is an event more recent than past week and in the correct group
        if filter_date(event) and filter_group(event, config.selected_groups):
            logging.debug('An event was added : {}'.format(event.get('summary')))
            calendar.add_component(event)

    # Copy tasks
    for todo in online_calendar.get_todos():
        logging.debug('A task was added : {}'.format(todo.get('summary')))
        calendar.add_component(todo)

    # Copy other components
    for component in online_calendar.get_other_components():
        logging.info('A unrecognised component ({}) was added : {}'.format(component.name, component.to_ical()))
        calendar.add_component(component)

# Write resulting calendar
with open('calendar.ics', 'wb') as f:
    f.write(calendar.to_ical())
