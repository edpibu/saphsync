# SaphSync

SaphSync permet de syncroniser son agenda SAPHIRE en :

 * Enlevant les TP, TD et BE qui ne nous concerne pas ;
 * Enlevant les évènements passés ;
 * En fussionannt tout en un unique calendrier au format ICS.

## Mise en place

Il faut créer `config.yml` à partir de `config.yml.example`.

Ensuite il faut donner son mot de passe ENS dans son trousseau de clé.
Pour cela :

```Python
import keyring
keyring.set_password('sogo', 'identifiant', 'mon_de_passe')
```

Ou sinon utiliser une application tel que Gnome Keyring en nommant l'entrée
`sogo`.

