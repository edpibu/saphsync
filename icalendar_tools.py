import logging
from datetime import datetime, date
from typing import Union

from icalendar import Calendar
from requests import get
from requests.auth import HTTPBasicAuth


class OnlineCalendar:
    logger = logging.getLogger('OnlineCalendar')

    def __init__(self, url: str, login: str, password: str):
        """Download and parse an icalendar file with authentication"""
        self.logger.info("Downloading {}".format(url))
        request = get(url, auth=HTTPBasicAuth(login, password))
        content = request.text

        # Parse components
        self.components = Calendar.from_ical(content)

    def get_events(self):
        """Return all calendar events"""
        for component in self.components.walk():
            if component.name == 'VEVENT':
                yield component

    def get_todos(self):
        """Return all calendar tasks"""
        for component in self.components.walk():
            if component.name == 'VTODO':
                yield component

    def get_timezone(self):
        """Return one timezone"""
        for component in self.components.walk():
            if component.name == 'VTIMEZONE':
                return component

    def get_other_components(self):
        """Return all unrecognised components"""
        blacklist = ['VEVENT', 'VTODO', 'VTIMEZONE', 'VCALENDAR', 'DAYLIGHT', 'STANDARD']

        for component in self.components.walk():
            if component.name not in blacklist:
                yield component


def get_event_start_date(event) -> Union[date, bool]:
    """Return start date of an event"""
    event_date = event.decoded('dtstart')

    if isinstance(event_date, datetime):
        return event_date.date()
    elif isinstance(event_date, date):
        return event_date
    else:
        return False
