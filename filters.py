import logging
import re
from datetime import date, timedelta

from icalendar_tools import get_event_start_date

logger = logging.getLogger('Filters')


def filter_date(event) -> bool:
    """Return true if event day is after the beginning of last week"""
    event_date = get_event_start_date(event)

    if event_date:
        from_date = date.today() - timedelta(weeks=1)
        return event_date >= from_date
    else:
        # If there is a bug in data then do not filter
        summary = event.get('summary')
        logger.warn('There was an issue with the date of {}'.format(summary))
        return True


def filter_group(event, selected_groups) -> bool:
    """Filter group according to the group selected"""
    summary = event.get('summary')
    summary_search = re.search('([0-9]{3}) - ([A-Z]{2}) \(grpe : ([^\)]*)\)', summary, re.IGNORECASE)

    if summary_search:
        ue, group = 'ue_' + summary_search.group(1), summary_search.group(3)
        if ue in selected_groups:
            # UE is configured
            if group in selected_groups[ue]:
                # Group was selected
                return True
            else:
                # Group is not the one selected
                logger.info('Removed {} because it had not been chosen'.format(summary))
                return False

    # If there is not a group specified do not filter
    return True
